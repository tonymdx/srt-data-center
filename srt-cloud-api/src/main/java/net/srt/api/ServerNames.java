package net.srt.api;

/**
 * 服务名称集合
 *
 * @author 阿沐 babamu@126.com
 */
public interface ServerNames {
    /**
     * srt-cloud-system 服务名
     */
    String SYSTEM_SERVER_NAME = "srt-cloud-system";
    /**
     * srt-cloud-message 服务名
     */
    String MESSAGE_SERVER_NAME = "srt-cloud-message";

	/**
	 * srt-cloud-quartz 服务名
	 */
	String QUARTZ_SERVER_NAME = "srt-cloud-quartz";

	/**
	 * srt-cloud-data-integrate 服务名
	 */
	String DATA_INTEGRATE_NAME = "srt-cloud-data-integrate";

	/**
	 * srt-cloud-data-development 服务名
	 */
	String DATA_DEVELOPMENT_NAME = "srt-cloud-data-development";
}
