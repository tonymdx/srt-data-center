package net.srt.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import net.srt.convert.DataOdsConvert;
import net.srt.dto.SqlConsole;
import net.srt.entity.DataOdsEntity;
import net.srt.framework.common.page.PageResult;
import net.srt.framework.common.utils.Result;
import net.srt.query.DataOdsQuery;
import net.srt.service.DataOdsService;
import net.srt.vo.ColumnDescriptionVo;
import net.srt.vo.DataOdsVO;
import net.srt.vo.SchemaTableDataVo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import srt.cloud.framework.dbswitch.core.model.ColumnDescription;

import javax.validation.Valid;
import java.util.List;

/**
 * 数据集成-贴源数据
 *
 * @author zrx 985134801@qq.com
 * @since 1.0.0 2022-11-07
 */
@RestController
@RequestMapping("ods")
@Tag(name = "数据集成-贴源数据")
@AllArgsConstructor
public class DataOdsController {
	private final DataOdsService dataOdsService;

	@GetMapping("page")
	@Operation(summary = "分页")
	public Result<PageResult<DataOdsVO>> page(@Valid DataOdsQuery query) {
		PageResult<DataOdsVO> page = dataOdsService.page(query);
		return Result.ok(page);
	}

	@GetMapping("{id}")
	@Operation(summary = "信息")
	public Result<DataOdsVO> get(@PathVariable("id") Long id) {
		DataOdsEntity entity = dataOdsService.getById(id);

		return Result.ok(DataOdsConvert.INSTANCE.convert(entity));
	}

	@GetMapping("/{id}/{tableName}/column-info")
	@Operation(summary = "获取字段信息")
	public Result<List<ColumnDescriptionVo>> columnInfo(@PathVariable Long id, @PathVariable String tableName) {
		return Result.ok(dataOdsService.getColumnInfo(id, tableName));
	}

	@GetMapping("/{id}/{tableName}/table-data")
	@Operation(summary = "获取表数据")
	public Result<SchemaTableDataVo> getTableData(@PathVariable Long id, @PathVariable String tableName) {
		return Result.ok(dataOdsService.getTableData(id, tableName));
	}

	@DeleteMapping
	@Operation(summary = "删除")
	public Result<String> delete(@RequestBody List<Long> idList) {
		dataOdsService.delete(idList);
		return Result.ok();
	}
}
