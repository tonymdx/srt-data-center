package net.srt.convert;

import net.srt.api.module.data.integrate.dto.DataOdsDto;
import net.srt.entity.DataOdsEntity;
import net.srt.vo.DataOdsVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 数据集成-贴源数据
 *
 * @author zrx 985134801@qq.com
 * @since 1.0.0 2022-11-07
 */
@Mapper
public interface DataOdsConvert {
	DataOdsConvert INSTANCE = Mappers.getMapper(DataOdsConvert.class);

	DataOdsEntity convert(DataOdsVO vo);

	DataOdsEntity convertByDto(DataOdsDto dto);

	DataOdsVO convert(DataOdsEntity entity);

	List<DataOdsVO> convertList(List<DataOdsEntity> list);

}
