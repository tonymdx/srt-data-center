package net.srt.service;

import net.srt.entity.DataOdsEntity;
import net.srt.framework.common.page.PageResult;
import net.srt.framework.mybatis.service.BaseService;
import net.srt.query.DataOdsQuery;
import net.srt.vo.ColumnDescriptionVo;
import net.srt.vo.DataOdsVO;
import net.srt.vo.SchemaTableDataVo;

import java.util.List;

/**
 * 数据集成-贴源数据
 *
 * @author zrx 985134801@qq.com
 * @since 1.0.0 2022-11-07
 */
public interface DataOdsService extends BaseService<DataOdsEntity> {

	PageResult<DataOdsVO> page(DataOdsQuery query);

	void save(DataOdsVO vo);

	void update(DataOdsVO vo);

	void delete(List<Long> idList);

	DataOdsEntity getByTableName(Long projectId, String tableName);

	List<ColumnDescriptionVo> getColumnInfo(Long id, String tableName);

	SchemaTableDataVo getTableData(Long id, String tableName);
}
