package net.srt.api;

import lombok.RequiredArgsConstructor;
import net.srt.api.module.data.integrate.DataDatabaseApi;
import net.srt.api.module.data.integrate.DataOdsApi;
import net.srt.api.module.data.integrate.dto.DataDatabaseDto;
import net.srt.api.module.data.integrate.dto.DataOdsDto;
import net.srt.convert.DataDatabaseConvert;
import net.srt.convert.DataOdsConvert;
import net.srt.entity.DataOdsEntity;
import net.srt.framework.common.utils.Result;
import net.srt.service.DataDatabaseService;
import net.srt.service.DataOdsService;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName DataDatabaseApiImpl
 * @Author zrx
 * @Date 2022/10/26 11:50
 */
@RestController
@RequiredArgsConstructor
public class DataDatabaseApiImpl implements DataDatabaseApi {

	private final DataDatabaseService databaseService;

	@Override
	public Result<DataDatabaseDto> getById(Long id) {
		return Result.ok(DataDatabaseConvert.INSTANCE.convertDto(databaseService.getById(id)));
	}
}
