import{a6 as t}from"./index.14bc3c0f.js";const o=s=>t.get("/sys/post/"+s),p=s=>s.id?t.put("/sys/post",s):t.post("/sys/post",s),i=()=>t.get("/sys/post/list");export{p as a,i as b,o as u};
