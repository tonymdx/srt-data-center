import{a6 as r}from"./index.14bc3c0f.js";const t=()=>r.get("/sys/org/list"),i=s=>r.get("/sys/org/"+s),u=s=>s.id?r.put("/sys/org",s):r.post("/sys/org",s);export{i as a,u as b,t as u};
